Source: qt6-quick3dphysics
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>,
           Simon Quigley <tsimonq2@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-pkgkde-symbolshelper,
               dh-sequence-qmldeps,
               cmake (>= 3.24~),
               ninja-build,
               pkg-kde-tools,
               pkgconf,
               qt6-base-dev (>= 6.8.2+dfsg~),
               qt6-base-private-dev (>= 6.8.2+dfsg~),
               qt6-declarative-dev (>= 6.8.2+dfsg~),
               qt6-declarative-private-dev (>= 6.8.2+dfsg~),
               qt6-quick3d-dev (>= 6.8.2~),
               qt6-quick3d-private-dev (>= 6.8.2~),
               qt6-shadertools-dev (>= 6.8.2~),
Standards-Version: 4.7.0
Homepage: https://www.qt.io/developers/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-quick3dphysics
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-quick3dphysics.git
Rules-Requires-Root: no

Package: libqt6quick3dphysics6
Architecture: amd64 arm64 armel armhf i386 mips64el
Multi-Arch: same
Depends: sse2-support [any-i386], ${misc:Depends}, ${shlibs:Depends},
Description: Qt 6 Quick 3D Physics library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt 6 Quick 3D Physics library.

Package: qml6-module-qtquick3d-physics
Architecture: amd64 arm64 armel armhf i386 mips64el
Multi-Arch: same
Depends: ${misc:Depends}, ${qml6:Depends}, ${shlibs:Depends},
Breaks: libqt6quick3dphysicshelpers6,
Replaces: libqt6quick3dphysicshelpers6,
Description: Qt 6 Quick 3D Physics QML module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides the QML module for Qt 6 Quick 3D Physics.

Package: qt6-quick3dphysics-dev
Section: libdevel
Architecture: amd64 arm64 armel armhf i386 mips64el
Multi-Arch: same
Depends: libqt6quick3dphysics6 (= ${binary:Version}),
         qml6-module-qtquick3d-physics (= ${binary:Version}),
         ${misc:Depends},
Description: Qt 6 Quick 3D Physics - development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files used for building Qt 6
 applications using Qt Quick 3D Physics library.

Package: qt6-quick3dphysics-dev-tools
Architecture: amd64 arm64 armel armhf i386 mips64el
Multi-Arch: foreign
Section: devel
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: Qt 6 Quick 3D Physics development programs
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the development programs used for building Qt 6
 Quick 3D Physics applications.

Package: qt6-quick3dphysics-examples
Architecture: amd64 arm64 armel armhf i386 mips64el
Multi-Arch: same
Section: misc
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: Qt 6 Quick 3D physics examples
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the examples for the Qt Quick 3D physics submodule.
